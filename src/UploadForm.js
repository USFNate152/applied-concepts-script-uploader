import React, { useEffect } from 'react';
import './App.css';
import {
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  Button
} from '@material-ui/core';
import scriptAPI from './API/scriptAPI';

function UploadForm() {
  const [ program, setProgram ] = React.useState('')
  const [ programName, setProgramName] = React.useState('')
  const [ actionCode, setActionCode ] = React.useState('')
  const [ file, setFile ] = React.useState('')
  const [ used, setUsed] = React.useState('')
  const [ type, setType] = React.useState('')
  const [ programList, setProgramList] = React.useState([])
  const [ actionList, setActionList] = React.useState([])

  const programSelect = React.createRef()
  const actionSelect = React.createRef()
  const typeSelect = React.createRef()
  const conditionSelect = React.createRef()

  const handleProgramSelect = async (event) => {
    setProgram(event.target.value)
    programList.forEach((program) => {
      if (program.value === event.target.value) {
        setProgramName(program.name)
      }
    })
    const response = await scriptAPI.getActionCodes(event.target.value);
    const actionList = [];
    response.data.forEach(action => {
      console.log(action)
      if (action.myCode === "INCPS" || action.myCode === "test") {
        return
      } else {
        actionList.push(action.myCode)
      }
    })
    setActionList(actionList)
  }

  const handleActionCodeSelect = async (event) => {
    const actionCode = event.target.value;
    setActionCode(actionCode)
  }

  const handleTypeSelect = (event) => {
    setType(event.target.value)
  }

  const handleUsedSelect = (event) => {
    setUsed(event.target.value)
  }

  const handleFileSelection = (event) => {
    setFile(event.target.files[0])
  }

  const handleFileUpload = async (programId, programName, actionCode, type, condition, file) => {
    let formData = new FormData();
    formData.append('file', file)
    formData.append('programId', programId)
    formData.append('actionCode', actionCode)
    formData.append('type', type)
    formData.append('condition', condition)
    formData.append('name', programName)
    try {
      await scriptAPI.uploadSelectedScript(formData)
      setActionCode('');
      setType('');
      setUsed('');
      setFile('');
      document.getElementById('upload-files').value = '';
      window.alert(`Successful Upload - \n Program: ${programName}\n ActionCode: ${actionCode}\n Type: ${type}\n Condition: ${condition}\n File: ${file.name}`)
    } catch(error) {
      console.log(error)
    }
  }

  useEffect(() => {
    async function getPrograms() {
      const response = await scriptAPI.getProgramCodes()
      const programList =  []
      response.data.forEach(program => {
        const programData = Object.assign({}, {name: program.NAME, value: program.PRODUCTID })
        programList.push(programData)
      })
      setProgramList(programList)
    }
    getPrograms();
  }, [])

  const programMenuItems = programList.map((program) => {
    return (
      <MenuItem key={programList.indexOf(program)} name={program.name} value={program.value}>{program.name}</MenuItem>
    )
  })

  const actionMenuItems = actionList.map((action) => {
    return (
      <MenuItem key={actionList.indexOf(action)} value={action}>{action}</MenuItem>
    )
  })

  return (
    <div className='App'>
      <div className='App--uploadForm'>
        <FormControl className='uploadForm--formControl' ref={programSelect}>
          <InputLabel>Program</InputLabel>
          <Select
          className='uploadForm--select'
          variant='outlined'
          value={program}
          onChange={handleProgramSelect}
          >
            {programMenuItems}
          </Select>
        </FormControl>
        <FormControl className='uploadForm--formControl'>
          <InputLabel>Action Code</InputLabel>
          <Select
          ref={actionSelect}
          className='uploadForm--select'
          variant='outlined'
          value={actionCode}
          onChange={handleActionCodeSelect}
          >
            {actionMenuItems}
          </Select>
        </FormControl>
        <FormControl className='uploadForm--formControl'>
          <InputLabel>Type</InputLabel>
          <Select
          ref={typeSelect}
          className='uploadForm--select'
          variant='outlined'
          value={type}
          onChange={handleTypeSelect}
          >
            <MenuItem value='cars'>Cars</MenuItem>
            <MenuItem value='motorsports'>Motorsports</MenuItem>
            <MenuItem value="both">Both</MenuItem>
          </Select>
        </FormControl>
        <FormControl className='uploadForm--formControl'>
          <InputLabel>New/Used</InputLabel>
          <Select
          ref={conditionSelect}
          className='uploadForm--select'
          variant='outlined'
          value={used}
          onChange={handleUsedSelect}
          >
            <MenuItem value='new'>New</MenuItem>
            <MenuItem value='used'>Used</MenuItem>
          </Select>
        </FormControl>
        <input id='upload-files' className='uploadForm--uploader' name='file' type='file' onChange={handleFileSelection} />
        <Button
        color='primary'
        variant='contained'
        className='uploadForm--upload-btn'
        onClick={() => handleFileUpload(program, programName, actionCode, type, used, file)}
        disabled={program === '' || actionCode === '' || type === '' || used === '' || file === ''}
        >
          Upload
        </Button>
      </div>
    </div>
  );
}

export default UploadForm;