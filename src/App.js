import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import UploadForm from './UploadForm';
import LoginPage from './Login';
import headerBannerImage from './Images/header-image.png'
import headerLogoImage from './Images/gear-head-only-white.png'
import footerLogoImage from './Images/ACI-Logo-Horz-WHITE.png'
import loginAPI from './API/loginAPI';
import { Button } from '@material-ui/core';

function App() {
  const [ isUserLoggedIn, setIsUserLoggedIn ] = React.useState(false)

  const checkUserLogin = (value) => {
    setIsUserLoggedIn(value)
  }

  const handleLogout = async () => {
    try {
      await loginAPI.logout()
      window.alert('Successfully logged out.')
      setIsUserLoggedIn(false)
    } catch (error) {
      window.alert('There was a problem logging you out. Please try again.')
    }
  }
  
  return (
    <Router>
      <div className='App'>
        <div className='App--header'>
          <img className='header--logo' src={headerLogoImage} alt='gearhead logo'/>
          <img className='header--banner' src={headerBannerImage} alt='header banner'/>
          {isUserLoggedIn &&
          <Button variant='contained' className='header--logoutBtn' onClick={handleLogout}>Logout</Button>}
        </div>
        <Switch>
          <Route exact path='/'>
            <LoginPage checkUserLogin={checkUserLogin} />
          </Route>
          <Route path='/upload'>
            {isUserLoggedIn ? <UploadForm /> : <Redirect to='/' />}
          </Route>
        </Switch>
        <div className='App--footer'>
          <img className='footer--logo' src={footerLogoImage} alt='Applied Concepts Logo' />
        </div>
      </div>
    </Router>
  );
}

export default App;
