import React from 'react';
import {
    Button,
    TextField
} from '@material-ui/core';
import loginAPI from './API/loginAPI';
import { useHistory } from 'react-router-dom';
import './App.css';

function LoginPage(props) {
    const [ username, setUsername ] = React.useState('')
    const [ password, setPassword ] = React.useState('')
    const history = useHistory();

    const handleChangeUsername = (event) => {
        setUsername(event.target.value)
    }

    const handleChangePassword = (event) => {
        setPassword(event.target.value)
    }

    const handleLogin = async () => {
        const payload = Object.assign({}, {username: username, password: password})
        try {
            const response = await loginAPI.login(payload);
            if (response.data.role === 'admin') {
                props.checkUserLogin(true)
                const token = btoa(username + ':' + password);
                sessionStorage.setItem('token', token)
                history.push('/upload')
            } else {
                props.checkUserLogin(false)
                window.alert('Administrative access required')
            }
        } catch(error) {
            window.alert('Incorrect Username or Password. Please try again.')
        }
    }

    return(
        <form>
            <div className='loginForm'>
                <TextField variant='outlined' className='loginForm--input' label='Username' onChange={handleChangeUsername} value={username}/>
                <TextField variant='outlined' className='loginForm--input' type='password' label='Password' onChange={handleChangePassword} value={password}/>
            </div>
            <Button className='loginForm--btn' variant='contained' color='primary' onClick={handleLogin}>Login</Button>
        </form>
    )
}

export default LoginPage