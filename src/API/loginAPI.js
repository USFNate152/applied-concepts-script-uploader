import apiClient from './apiClient'

export default {
    login(payload) {
        const token = btoa(payload.username + ':' + payload.password)
        return apiClient.get('/user/login', {
            headers: {
                'Content-Type': 'application/json',
                authorization: 'Basic ' + token
            }
        });
    },
    logout() {
        return apiClient.post('/user/logout')
    }
}