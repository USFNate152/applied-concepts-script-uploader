import apiClient from './apiClient';

export default {
    getProgramCodes() {
        const token = sessionStorage.getItem('token')
        return apiClient.post('/all-programs/',
        {},
        {
            headers: {
                'Content-Type': 'application/json',
                authorization: 'Basic ' + token
            }
        })
    },
    getActionCodes(programId) {
        const token = sessionStorage.getItem('token')
        return apiClient.post('/program-action-codes/', 
        {
            programId: programId
        },
        {
            headers: {
                'Content-Type': 'application/json',
                authorization: 'Basic ' + token
            }
        })
    },
    uploadSelectedScript(payload) {
        const token = sessionStorage.getItem('token')
        return apiClient.post('/update-script/', 
        payload,
        {
            headers: {
                authorization: 'Basic ' + token
            }
        })
    }
}